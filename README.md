## Wikibugs

wikibugs is an IRC bot that idles in the #wikimedia-dev connect development channel (and others) on the Libera Chat IRC network.

The bot polls the phabricator API, listens to the Gerrit events feed, and posts changes to tasks and patches into various IRC channels. 

Further documentation, including deployment instructions, can be found on [the "wikibugs" page on Wikitech](https://www.mediawiki.org/wiki/Wikibugs).

